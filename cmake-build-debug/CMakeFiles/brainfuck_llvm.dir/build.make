# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.17

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /opt/clion/bin/cmake/linux/bin/cmake

# The command to remove a file.
RM = /opt/clion/bin/cmake/linux/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/cmake-build-debug

# Include any dependencies generated for this target.
include CMakeFiles/brainfuck_llvm.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/brainfuck_llvm.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/brainfuck_llvm.dir/flags.make

CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.o: CMakeFiles/brainfuck_llvm.dir/flags.make
CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.o: ../brainfuck_llvm.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/cmake-build-debug/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.o"
	/usr/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.o -c /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/brainfuck_llvm.cpp

CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.i"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/brainfuck_llvm.cpp > CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.i

CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.s"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/brainfuck_llvm.cpp -o CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.s

# Object files for target brainfuck_llvm
brainfuck_llvm_OBJECTS = \
"CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.o"

# External object files for target brainfuck_llvm
brainfuck_llvm_EXTERNAL_OBJECTS =

brainfuck_llvm: CMakeFiles/brainfuck_llvm.dir/brainfuck_llvm.cpp.o
brainfuck_llvm: CMakeFiles/brainfuck_llvm.dir/build.make
brainfuck_llvm: /usr/lib/libLLVMSupport.a
brainfuck_llvm: /usr/lib/libLLVMCore.a
brainfuck_llvm: /usr/lib/libLLVMIRReader.a
brainfuck_llvm: /usr/lib/libLLVMOrcJIT.a
brainfuck_llvm: /usr/lib/libLLVMX86CodeGen.a
brainfuck_llvm: /usr/lib/libLLVMX86AsmParser.a
brainfuck_llvm: /usr/lib/libLLVMX86Desc.a
brainfuck_llvm: /usr/lib/libLLVMX86Disassembler.a
brainfuck_llvm: /usr/lib/libLLVMX86Info.a
brainfuck_llvm: /usr/lib/libLLVMExecutionEngine.a
brainfuck_llvm: /usr/lib/libLLVMJITLink.a
brainfuck_llvm: /usr/lib/libLLVMOrcError.a
brainfuck_llvm: /usr/lib/libLLVMPasses.a
brainfuck_llvm: /usr/lib/libLLVMCoroutines.a
brainfuck_llvm: /usr/lib/libLLVMipo.a
brainfuck_llvm: /usr/lib/libLLVMIRReader.a
brainfuck_llvm: /usr/lib/libLLVMAsmParser.a
brainfuck_llvm: /usr/lib/libLLVMInstrumentation.a
brainfuck_llvm: /usr/lib/libLLVMVectorize.a
brainfuck_llvm: /usr/lib/libLLVMFrontendOpenMP.a
brainfuck_llvm: /usr/lib/libLLVMLinker.a
brainfuck_llvm: /usr/lib/libLLVMRuntimeDyld.a
brainfuck_llvm: /usr/lib/libLLVMAsmPrinter.a
brainfuck_llvm: /usr/lib/libLLVMDebugInfoDWARF.a
brainfuck_llvm: /usr/lib/libLLVMCFGuard.a
brainfuck_llvm: /usr/lib/libLLVMGlobalISel.a
brainfuck_llvm: /usr/lib/libLLVMSelectionDAG.a
brainfuck_llvm: /usr/lib/libLLVMCodeGen.a
brainfuck_llvm: /usr/lib/libLLVMTarget.a
brainfuck_llvm: /usr/lib/libLLVMBitWriter.a
brainfuck_llvm: /usr/lib/libLLVMScalarOpts.a
brainfuck_llvm: /usr/lib/libLLVMAggressiveInstCombine.a
brainfuck_llvm: /usr/lib/libLLVMInstCombine.a
brainfuck_llvm: /usr/lib/libLLVMTransformUtils.a
brainfuck_llvm: /usr/lib/libLLVMAnalysis.a
brainfuck_llvm: /usr/lib/libLLVMObject.a
brainfuck_llvm: /usr/lib/libLLVMBitReader.a
brainfuck_llvm: /usr/lib/libLLVMTextAPI.a
brainfuck_llvm: /usr/lib/libLLVMProfileData.a
brainfuck_llvm: /usr/lib/libLLVMCore.a
brainfuck_llvm: /usr/lib/libLLVMRemarks.a
brainfuck_llvm: /usr/lib/libLLVMBitstreamReader.a
brainfuck_llvm: /usr/lib/libLLVMMCParser.a
brainfuck_llvm: /usr/lib/libLLVMMCDisassembler.a
brainfuck_llvm: /usr/lib/libLLVMMC.a
brainfuck_llvm: /usr/lib/libLLVMBinaryFormat.a
brainfuck_llvm: /usr/lib/libLLVMDebugInfoCodeView.a
brainfuck_llvm: /usr/lib/libLLVMDebugInfoMSF.a
brainfuck_llvm: /usr/lib/libLLVMSupport.a
brainfuck_llvm: /usr/lib/libLLVMDemangle.a
brainfuck_llvm: CMakeFiles/brainfuck_llvm.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/cmake-build-debug/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable brainfuck_llvm"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/brainfuck_llvm.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/brainfuck_llvm.dir/build: brainfuck_llvm

.PHONY : CMakeFiles/brainfuck_llvm.dir/build

CMakeFiles/brainfuck_llvm.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/brainfuck_llvm.dir/cmake_clean.cmake
.PHONY : CMakeFiles/brainfuck_llvm.dir/clean

CMakeFiles/brainfuck_llvm.dir/depend:
	cd /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/cmake-build-debug && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/cmake-build-debug /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/cmake-build-debug /home/lars/Desktop/Uni/Progs/C++/brainfuck-llvm/cmake-build-debug/CMakeFiles/brainfuck_llvm.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/brainfuck_llvm.dir/depend

