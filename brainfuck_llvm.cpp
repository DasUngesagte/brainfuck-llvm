#include "llvm/ADT/APFloat.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include <cctype>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <vector>

//===----------------------------------------------------------------------===//
// Lexer
//===----------------------------------------------------------------------===//

enum Token : int {
    tok_greater = '>',   // >
    tok_lesser = '<',    // <
    tok_plus = '+',      // +
    tok_minus = '-',     // -
    tok_dot = '.',       // .
    tok_comma = ',',     // ,
    tok_sbc = ']',       // ]
    tok_sbo = '[',       // [
    tok_eof = -1        // EOF
};

static int gettok() {
    static int LastChar = ' ';

    // Skip any whitespace.
    while (isspace(LastChar))
        LastChar = getchar();

    if (LastChar == '>') {
        LastChar = getchar();
        return tok_greater;
    } else if (LastChar == '<') {
        LastChar = getchar();
        return tok_lesser;
    } else if (LastChar == '+') {
        LastChar = getchar();
        return tok_plus;
    } else if (LastChar == '-') {
        LastChar = getchar();
        return tok_minus;
    } else if (LastChar == '.') {
        LastChar = getchar();
        return tok_dot;
    } else if (LastChar == ',') {
        LastChar = getchar();
        return tok_comma;
    } else if (LastChar == ']') {
        LastChar = getchar();
        return tok_sbc;
    } else if (LastChar == '[') {
        LastChar = getchar();
        return tok_sbo;
    } else if (LastChar == EOF) {
        LastChar = getchar();
        return tok_eof;
    }

    int ThisChar = LastChar;
    LastChar = getchar();
    return ThisChar;
}

//===----------------------------------------------------------------------===//
// Abstract Syntax Tree (aka Parse Tree)
//===----------------------------------------------------------------------===//

namespace {

/// ExprAST - Base class for all expression nodes.
    class ExprAST {
    public:
        virtual ~ExprAST() = default;

        virtual llvm::Value *codegen() = 0;
    };

/// OpExprAST - Expression class for an operator.
    class OpExprAST : public ExprAST {
        char Op;
        //std::unique_ptr<ExprAST> LHS, RHS;

    public:
        explicit OpExprAST(char Op)
                : Op(Op) {}

        llvm::Value *codegen() override;
    };

    class LoopExprAST : public ExprAST {
        std::unique_ptr<std::vector<std::unique_ptr<ExprAST>>> Body;

    public:
        explicit LoopExprAST(std::unique_ptr<std::vector<std::unique_ptr<ExprAST>>> Body)
                : Body(std::move(Body)){}

        llvm::Value *codegen() override;
    };
} // end anonymous namespace


//===----------------------------------------------------------------------===//
// Parser
//===----------------------------------------------------------------------===//


std::unique_ptr<ExprAST> LogError(const char *Str) {
    fprintf(stderr, "Error: %s\n", Str);
    return nullptr;
}

int CurTok;
static int getNextToken() { return CurTok = gettok(); }

static std::unique_ptr<ExprAST> ParsePrimary();

//parseOp
static std::unique_ptr<ExprAST> ParseOP() {
    auto Result = std::make_unique<OpExprAST>(CurTok);
    getNextToken();
    return std::move(Result);
}

/// forexpr ::= '[' (expression)* ']'
static std::unique_ptr<ExprAST> ParseLoopExpr() {

    getNextToken();  // eat the [.

    auto Body = std::make_unique<std::vector<std::unique_ptr<ExprAST>>>();

    // Build a list of expressions in the loop:
    while(true){
        auto expr = ParsePrimary();
        if (!expr)
            return nullptr;

        Body->push_back(std::move(expr));

        if(CurTok == tok_sbc){
            getNextToken(); // Eat [
            break;
        }
    }

    return std::make_unique<LoopExprAST>(std::move(Body));
}

static std::unique_ptr<ExprAST> ParsePrimary(){
    switch (CurTok) {
        case tok_sbo:
            return ParseLoopExpr();
        default:
            return ParseOP();
    }
}

//===----------------------------------------------------------------------===//
// Code Generation
//===----------------------------------------------------------------------===//

static std::unique_ptr<llvm::LLVMContext> TheContext;
static std::unique_ptr<llvm::Module> TheModule;
static std::unique_ptr<llvm::IRBuilder<>> Builder;
static std::unique_ptr<llvm::legacy::FunctionPassManager> TheFPM;

// Some global vars we need for brainfuck specifically
llvm::Value *stack;
uint64_t stack_pos = 0;
const uint64_t stack_size = 30000;
llvm::Function *mainFunc;
llvm::Value *format_specifier;

llvm::Value *LogErrorV(const char *Str) {
    LogError(Str);
    return nullptr;
}

llvm::Value *HandleMovePtr(int8_t diff) {
    if (stack_pos + diff >= stack_size)
        stack_pos = 0;
    else if (stack_pos + diff < 0)
        stack_pos = stack_size - 1;
    else
        stack_pos += diff;
    llvm::Value* V = llvm::ConstantInt::get(llvm::Type::getInt8Ty(*TheContext), 0);
    return Builder->CreateAdd(V,V, "nop");
}

llvm::Value *HandleChangeVal(int8_t diff) {

    // Get address:
    auto offset = llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(TheModule->getContext()), stack_pos);
    llvm::Value *indices[2] = {Builder->getInt64(0), offset};
    auto ptr = Builder->CreateInBoundsGEP(stack, llvm::ArrayRef<llvm::Value *>(indices, 2));

    // Get the value:
    auto val = Builder->CreateLoad(llvm::IntegerType::getInt8Ty(*TheContext), ptr, "val");
    auto add = Builder->CreateAdd(val, Builder->getInt8(diff));
    return Builder->CreateStore(add, ptr);
}

llvm::Value *HandleOutput() {
    auto print_function = TheModule->getFunction("printf");

    std::vector<llvm::Value *> print_args;
    print_args.push_back(format_specifier);

    auto offset = llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(TheModule->getContext()), stack_pos);
    llvm::Value *indices[2] = {Builder->getInt64(0), offset};
    auto ptr = Builder->CreateInBoundsGEP(stack, llvm::ArrayRef<llvm::Value *>(indices, 2));
    auto val = Builder->CreateLoad(llvm::IntegerType::getInt8Ty(*TheContext), ptr, "val");
    print_args.push_back(val);
    return Builder->CreateCall(print_function, print_args, "callprintf");
}

llvm::Value *HandleInput() {
    return nullptr;
}

llvm::Value *OpExprAST::codegen() {
    switch (Op) {
        case tok_greater:
            return HandleMovePtr(1);
        case tok_lesser:
            return HandleMovePtr(-1);
        case tok_plus:
            return HandleChangeVal(1);
        case tok_minus:
            return HandleChangeVal(-1);
        case tok_dot:
            return HandleOutput();
        case tok_comma:
            return HandleInput();
        default:
            return LogErrorV("invalid operator");
    }
}

llvm::Value *LoopExprAST::codegen() {

    auto LoopEntry = llvm::BasicBlock::Create(*TheContext, "entry", mainFunc);
    auto LoopBody = llvm::BasicBlock::Create(*TheContext, "loop", mainFunc);
    auto LoopExit = llvm::BasicBlock::Create(*TheContext, "exit", mainFunc);
    Builder->CreateBr(LoopEntry);

    // Handle Entry:
    Builder->SetInsertPoint(LoopEntry);
    auto offset = llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(TheModule->getContext()), stack_pos);
    llvm::Value *indices[2] = {Builder->getInt64(0), offset};
    auto ptr = Builder->CreateInBoundsGEP(stack, llvm::ArrayRef<llvm::Value *>(indices, 2));
    auto val = Builder->CreateLoad(llvm::IntegerType::getInt8Ty(*TheContext), ptr, "val");
    auto res = Builder->CreateICmpNE(
            val, Builder->getInt8(0), "loopcond");

    Builder->CreateCondBr(res, LoopBody, LoopExit);

    // Handle Body:
    Builder->SetInsertPoint(LoopBody);
    for(auto&& expr: *Body){
        expr->codegen();
    }
    Builder->CreateBr(LoopEntry);

    // Handle Exit:
    Builder->SetInsertPoint(LoopExit);


}

//===----------------------------------------------------------------------===//
// Top-Level parsing and JIT Driver
//===----------------------------------------------------------------------===//

static void InitializeModule() {
    // Open a new context and module.
    TheContext = std::make_unique<llvm::LLVMContext>();
    TheModule = std::make_unique<llvm::Module>("brainfuck", *TheContext);
    TheFPM = std::make_unique<llvm::legacy::FunctionPassManager>(TheModule.get());

    // Do simple "peephole" optimizations and bit-twiddling optzns.
    TheFPM->add(llvm::createInstructionCombiningPass());
    // Reassociate expressions.
    TheFPM->add(llvm::createReassociatePass());
    // Eliminate Common SubExpressions.
    TheFPM->add(llvm::createGVNPass());
    // Simplify the control flow graph (deleting unreachable blocks, etc).
    TheFPM->add(llvm::createCFGSimplificationPass());

    TheFPM->doInitialization();

    // Create a new builder for the module.
    Builder = std::make_unique<llvm::IRBuilder<>>(*TheContext);
}

static void HandleTopLevelExpression() {
    if(auto E = ParsePrimary()){
        E->codegen();
    } else {
        // Skip token for error recovery.
        getNextToken();
    }
}

static void MainLoop() {
    while (true) {
        switch (CurTok) {
            case tok_eof:
                return;
            default:
                HandleTopLevelExpression();
                break;
        }
    }
}

//===----------------------------------------------------------------------===//
// Main driver code.
//===----------------------------------------------------------------------===//

int main() {

    // Make the module, which holds all the code.
    InitializeModule();

    // Create a main function, in which our code will be inserted:
    mainFunc = llvm::Function::Create(
            llvm::FunctionType::get(llvm::Type::getInt32Ty(*TheContext), false),
            llvm::Function::ExternalLinkage, "main", TheModule.get());

    // Create a main block:
    auto block = llvm::BasicBlock::Create(TheModule->getContext(), "start", mainFunc);
    Builder->SetInsertPoint(block);

    // Create the array:
    llvm::Type *int_type = llvm::IntegerType::getInt8Ty(*TheContext);
    auto *arrayType = llvm::ArrayType::get(int_type, stack_size);
    stack = Builder->CreateAlloca(arrayType);

    // Ready a memset call:
    auto arg_0 = Builder->CreateBitCast(stack, llvm::IntegerType::getInt8PtrTy(*TheContext));
    Builder->CreateMemSet(arg_0, Builder->getInt8(0), Builder->getInt64(stack_size), llvm::MaybeAlign(16));

    // Create printf-function:
    format_specifier = Builder->CreateGlobalStringPtr("Output: %d\n", "format_specifier");
    llvm::FunctionType *FT = llvm::FunctionType::get(llvm::IntegerType::get(*TheContext, 32), true);
    llvm::Function *F =
            llvm::Function::Create(FT, llvm::Function::ExternalLinkage, "printf", TheModule.get());
    F->setCallingConv(llvm::CallingConv::C);


    // Run the main "interpreter loop" now:
    getNextToken();
    MainLoop();

    // Pointless return:
    Builder->CreateRet(Builder->getInt32(0));

    // Optimize the function.
    TheFPM->run(*mainFunc);

    // Print the llvm-ir:
    TheModule->print(llvm::outs(), nullptr);

    return 0;
}